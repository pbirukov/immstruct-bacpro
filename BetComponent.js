BetComponent = function () {
    return {
        references: null,
        requiredReferences: ["gamePhase"],
        render: function () {
            var self = this,
                disabledClass = self.references.gamePhase.cursor().deref() === "betting" ? "" : "disabled=\"disabled\"";
            $("#bet").html($("<button " + disabledClass + ">Bet 1 €</button>").click(function (){
                state.cursor("balance").update(function(val){
                    return val - 1;
                });
            }));
        }
    }
}();