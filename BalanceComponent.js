BalanceComponent = (function () {
    return {
        requiredReferences: ["balance"],
        references: null,
        render: function () {
            $("#balance").text(this.references.balance.cursor().deref());
        }
    }
}());