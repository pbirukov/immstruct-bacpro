$(function () {
    window.state = immstruct.withHistory("state", {
        gamePhase: "betting",
        user: {
            name: "foo bar"
        },
        balance: 1230.4,
        currencySym: "€"
    });

// relations between data path and component
    var bindings = {};


// init component. Provide required cursor and register binding
    var initComponent = function (cmp) {
        cmp.requiredReferences.forEach(function (path) {
            bindings[path] = bindings[path] || [];
            bindings[path].push(cmp);
            cmp.references = cmp.references || {};
            cmp.references[path] = state.reference(path);
        });
    };

    initComponent(BalanceComponent);
    BalanceComponent.render();
    initComponent(BetComponent);
    BetComponent.render();


// something was changed.
    state.on("swap", function (newState, oldState, path) {

        // re-render bound components
        (bindings[path] || []).forEach(function (cmp) {
            cmp.render();
        });

        // re-render canvas

    });

});

// iteractions
// it might be a reaction to WS event
function bettingOver() {
    state.cursor("gamePhase").update(function () {
        return "not betting";
    });
}
function bettingStarted() {
    state.cursor("gamePhase").update(function () {
        return "betting";
    });
}